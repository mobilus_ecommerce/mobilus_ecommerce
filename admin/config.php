<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/openCart/admin/');
define('HTTP_CATALOG', 'http://localhost/openCart/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/openCart/admin/');
define('HTTPS_CATALOG', 'http://localhost/openCart/');

// DIR
define('DIR_APPLICATION', 'C:/xampp/htdocs/openCart/admin/');
define('DIR_SYSTEM', 'C:/xampp/htdocs/openCart/system/');
define('DIR_IMAGE', 'C:/xampp/htdocs/openCart/image/');
define('DIR_LANGUAGE', 'C:/xampp/htdocs/openCart/admin/language/');
define('DIR_TEMPLATE', 'C:/xampp/htdocs/openCart/admin/view/template/');
define('DIR_CONFIG', 'C:/xampp/htdocs/openCart/system/config/');
define('DIR_CACHE', 'C:/xampp/htdocs/openCart/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/xampp/htdocs/openCart/system/storage/download/');
define('DIR_LOGS', 'C:/xampp/htdocs/openCart/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/xampp/htdocs/openCart/system/storage/modification/');
define('DIR_UPLOAD', 'C:/xampp/htdocs/openCart/system/storage/upload/');
define('DIR_CATALOG', 'C:/xampp/htdocs/openCart/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'opencart');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
